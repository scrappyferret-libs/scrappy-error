<?php

    // Require the database config including $servername, $username, $password, and $dbname
    require( 'database.php' );

    // Create connection
    $conn = new mysqli( $servername, $username, $password, $dbname );

    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    $body = file_get_contents('php://input');

    $error = json_decode( $body, true );

    $date = $error[ "date" ];
    $appName = $error[ "appName" ];
    $message = mysqli_real_escape_string( $conn, $error[ "message" ] );
    $stackTrace = mysqli_real_escape_string( $conn, $error[ "stackTrace" ] );

    $platform = $error[ "platform" ];
    $platformVersion = $error[ "platformVersion" ];
    $manufacturer = $error[ "manufacturer" ];
    $model = $error[ "model" ];
    $targetAppStore = $error[ "targetAppStore" ];
    $appVersionString = $error[ "appVersionString" ];
    $architectureInfo = $error[ "architectureInfo" ];
    $solarBuild = $error[ "solarBuild" ];
    $textureMemoryUsed = $error[ "textureMemoryUsed" ];

    $stage = $error[ "stage" ];
    $settings = $error[ "settings" ];
    $build = $error[ "build" ];

    function errorExists( $conn, $message ) {

        $result = $conn->query( "SELECT message from error where message = '$message' ");

        return $result->num_rows > 0;
    }

    if ( errorExists( $conn, $message ) ) {
        echo 1;
    } else {

        $sql = "INSERT INTO error ( id, date, appName, message, stackTrace, platform, platformVersion, manufacturer, model, targetAppStore, appVersionString, architectureInfo, solarBuild, textureMemoryUsed, stage, settings, build )
        VALUES ( null, '$date', '$appName', '$message', '$stackTrace', '$platform', '$platformVersion', '$manufacturer', '$model', '$targetAppStore', '$appVersionString', '$architectureInfo', '$solarBuild', '$textureMemoryUsed', '$stage', '$settings', '$build' )";

        if ($conn->query($sql) === TRUE) {
          echo 1;
        } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
        }

    }



    $conn->close();

?>
