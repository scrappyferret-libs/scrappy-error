--- Required libraries.
local json = require( "json" )
local lfs = require( "lfs" )

-- Localised functions.
local encode = json.encode
local open = io.open
local close = io.close
local date = os.date
local time = os.time
local remove = os.remove
local pathForFile = system.pathForFile
local insert = table.insert
local find = string.find
local len = string.len
local sub = string.sub

-- Localised values

-- Static values.

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Set the api url if required
	self:setAPIUrl( self._params.apiUrl )

	-- Set the onError callback, if required
	self:setCallback( self._params.onError )

    -- Register the handler for unhandled errors
    Runtime:addEventListener( "unhandledError", self )

	-- Set up the path for the log file
    self._logPath = pathForFile( "scrappy.scrappyError", system.DocumentsDirectory )

	-- Table to hold any listener functions
	self._listeners = {}

	-- Error file extension
	self._extension = ".error"

	-- Calculate the length of a crash file name
	self._filenameLength = len( time() .. self._extension )

	-- Submit any locally stored crash files that haven't been submitted yet
	self:_submitCrashFiles()

end

-- Sets the url for the external API that errors are submitted to.
-- @param url The url.
function library:setAPIUrl( url )
	self._apiURL = url
end

--- Gets the api url, if set.
-- @return The url.
function library:getAPIUrl()
	return self._apiURL
end

-- Sets the onError callback.
-- @param callback The callback.
function library:setCallback( callback )
	self._callback = callback
end

--- Gets the onError, if set.
-- @return The callback.
function library:getCallback()
	return self._callback
end

-- Add an event listener for unhandled errors.
-- @param listener The listener callback.
function library:addEventListener( listener )
	self._listeners[ #self._listeners + 1 ] = listener
end

--- Handler function for unhandled errors.
-- @param event The event table.
function library:unhandledError( event )

	-- Create the error table from the event
	local error = self:_createError( event )

	-- Fire any registered listeners
	self:_fireListeners( error, event)

	if not self._params.disableFileWriting then
		
		-- Create a error file and get the filename and path
		local filename, path = self:_createErrorFile( error )

		-- Submit the error to an online api
		self:_submitError( filename, path )
		
	end

	-- Create the log message
	local log = self:_createLogMessage( error )

	-- Log the error
    self:log( log )

	-- Return true to mark it as handled
	return true

end

--- Submits a message to the log.
-- @param message The message to log.
function library:log( message )

	-- Do we have a message?
	if message and not self._params.disableFileWriting then

	    -- Open the file for appending
	    local file, errorString = open( self._logPath, "a" )

		-- Do we have a file handle?
	    if file then

			-- Write message to the file
		   file:write( "\n" .. message )

		   -- Close the file handle
		   close( file )

	    else

 	        -- Error occurred; output the cause
 	        print( "File error: " .. errorString )

	    end

		-- Nil out the handle
	    file = nil

	end

end

--- Cleans a stacktrace for easier reading.
-- @param stackTrace The stack trace.
-- @return The cleaned stacktrace.
function library:_cleanStack( stackTrace )

    if stackTrace then

        local stack = stackTrace:gsub( "stack traceback:", "" )

        stack = stack:gsub( "^%s*(.-)%s*$", "%1" )

        return stack

    end

end

--- Creates an error table.
-- @param event The error event.
-- @return The error table.
function library:_createError( event )

	-- Create the default error
	local error =
	{
        date = date( "%c" ),
		message = event.errorMessage,
		stackTrace = self:_cleanStack( event.stackTrace ),
        appName = system.getInfo( "appName" ),
        appVersionString = system.getInfo( "appVersionString" ),
        architectureInfo = system.getInfo( "architectureInfo" ),
        solarBuild = system.getInfo( "build" ),
        textureMemoryUsed = system.getInfo( "textureMemoryUsed" ),
        manufacturer = system.getInfo( "manufacturer" ),
        model = system.getInfo( "model" ),
        platform = system.getInfo( "platform" ),
        platformVersion = system.getInfo( "platformVersion" ),
        targetAppStore = system.getInfo( "targetAppStore" )
	}

	-- Do we have a valid callback function?
	if self._callback and type( self._callback ) == "function" then

		-- Call it and get the extra stuff, then loop through it
		for k, v in pairs( self._callback( event ) or {} ) do

			-- Storing the paramaters in our error table
			error[ k ] = v

		end

	end

	-- Return it
	return error

end

--- Creates a log message.
-- @param error The error table.
-- @return The log message.
function library:_createLogMessage( error )

	local log = "----------------------------------------------------------------------------------------------------"

	log = log .. "\n\n"
    log = log .. error.date .." - " .. error.appName .. " " .. error.message .. "\n" .. error.stackTrace .. "\n\n"

	log = log .. "App Version: " .. error.appVersionString .. "\n"
	log = log .. "System Arch: " .. error.architectureInfo .. "\n"
	log = log .. "Solar Build: " .. error.solarBuild .. "\n"
	log = log .. "Tex Mem: " .. error.textureMemoryUsed .. "\n"
	log = log .. "Manufacturer: " .. error.manufacturer .. "\n"
	log = log .. "Model: " .. error.model .. "\n"
	log = log .. "Platform: " .. error.platform .. "\n"
	log = log .. "Platform Version: " .. error.platformVersion .. "\n"
	log = log .. "App Store: " .. error.targetAppStore .. "\n"

	return log

end

--- Fires any registered listeners.
-- @param error The error table.
-- @param event The error event.
function library:_fireListeners( error, event )

	-- Loop through any listeners
	for i = 1, #( self._listeners or {} ), 1 do

		-- Are they functions?
		if type( self._listeners[ i ] ) == "function" then

			-- Call them, passing in the error table and event
			self._listeners[ i ]( error, event )

		end

	end

end

--- Creates an error file and saves it to disk.
-- @param error The error table.
-- @return The filename and path.
function library:_createErrorFile( error )

	-- Do we have an error?
	if error and not self._params.disableFileWriting then

		-- Generate a filename for the local error file
		local filename = time() .. ".error"

		-- Create the path
		local path = pathForFile( filename, system.DocumentsDirectory )

		-- Open the file for appending
		local file, errorString = open( path, "w" )

		-- Do we have a file handle?
		if file then

			-- Andcode the error and write it to the file
		   file:write( encode( error ) )

		   -- Close the file handle
		   close( file )

		else

			-- Error occurred; output the cause
			print( "File error: " .. errorString )

		end

		-- Nil out the handle
		file = nil

		-- Return the filename and path
		return filename, path

	end

end

--- Submits the error to an online API url, if registered.
-- @param error The error table.
function library:_submitError( filename, path )

	-- Do we have an api url?
	if self._apiURL and system.getInfo( "platform" ) ~= "nx64" then

		-- Listener for network calls
		local function networkListener( event )

			-- Was there an error? ( Oh the ironing! )
			if event.isError then
				print( "Network error: ", event.response )
			else

				-- Was the response a 1? i.e. successfullys submitted
				if event.response == "1" then

					-- Delete the crash file as it was submitted successfully
					remove( path )

				end

			end
		end

		-- Finally upload it
		network.upload(
			self._apiURL,
			"POST",
			networkListener,
			filename,
			system.DocumentsDirectory,
			"application/json"
		)

	end

end

--- Submits any local crash files that haven't been submitted yet.
function library:_submitCrashFiles()

	if system.getInfo( "platform" ) ~= "nx64" then
		
		-- Calculate the correct dir seperator
		local seperator = package.config:sub( 1, 1 )

		function listfiles( dir, list )
			list = list or {}	-- use provided list or create a new one

			if dir then
				for entry in lfs.dir(dir) do
					if entry ~= "." and entry ~= ".." then
						local ne = dir .. seperator .. entry
						if lfs.attributes( ne ).mode == 'directory' then
							listfiles( ne,list )
						else
							insert( list, ne )
						end
					end
				end
			end

			return list

		end

		-- Loop through all files in the directory
		for _, fullPath in ipairs( listfiles( dirPath ) ) do

			-- Is this an error file?
			if find( fullPath, self._extension ) then

				-- Calculate the filename
				local filename = sub( fullPath, -self._filenameLength )

				-- Calculate the file path
				local path = pathForFile( filename, system.DocumentsDirectory )

				-- Submit the error online
				self:_submitError( filename, path )

			end

		end

	end

end

--- Destroys this library.
function library:destroy()

    -- Unregister the handler for unhandled errors
    Runtime:removeEventListener( "unhandledError", self )

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Error library
if not Scrappy.Error then

	-- Then store the library out
	Scrappy.Error = library

end

-- Return the new library
return library
