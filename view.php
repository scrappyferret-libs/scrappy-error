<meta http-equiv="refresh" content="5; URL=https://www.glitch.games/scrappyErrors/view.php">

<?php

    // Require the database config including $servername, $username, $password, and $dbname
    require( 'database.php' );

    // Create connection
    $conn = new mysqli($servername, $username, $password, $dbname);
    // Check connection
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM error ORDER BY id DESC";
    $result = $conn->query($sql);

    echo  "<center><h1>Errors: " . $result->num_rows  . "</center></h1><br><br>";

    if ($result->num_rows > 0) {

      // output data of each row
      while($row = $result->fetch_assoc()) {

          foreach ($row as $key => $value) {
            echo "<b>$key:</b> " . $value . "<br>";
        }

        echo "<br><br>";

        //echo "<b>ID:</b> " . $row["id"]. "<br><b>App Name:</b> " . $row["appName"]. "<br><b>Message:</b> " . $row["message"] . "<br><b>Stack:</b> " . $row["stackTrace"];
      }

    } else {

    }
    $conn->close();

    //id, date, appName, message, stackTrace, platform, platformVersion, manufacturer, model, targetAppStore, appVersionString, architectureInfo, solarBuild, textureMemoryUsed, stage, settings
?>
